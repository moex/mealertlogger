//
//  AppDelegate.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 21.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

