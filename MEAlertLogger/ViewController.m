//
//  ViewController.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 21.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "ViewController.h"
#import "MEAlertLogger.h"

typedef enum {
    ME_ALERT_WARN=1,
    ME_ALERT_NETWORK_OK=2,
    ME_ALERT_NETWORK_LOST=3,
    ME_ALERT_FATAL=4
} MEAlertType;

@interface MEAlertLoggerTest: MEAlertLogger <MEAlertLoggerDelegate>
@end

@implementation MEAlertLoggerTest
{
    NSDictionary *icons;
}

- (id) init{
    self = [super init];
        
    NSLog(@"LOGGER: %@", self);
    
    if (self) {
        icons = @{
                  @1: [UIImage imageNamed:@"alert_warn"],
                  @2: [UIImage imageNamed:@"alert_network_ok"],
                  @3: [UIImage imageNamed:@"alert_network_lost"],
                  @4: [UIImage imageNamed:@"alert_fatal"]
                  };
        self.delegate = self;
    }
    
    return self;
}

+ (id) sharedInstance{
    
    static dispatch_once_t onceToken;
    static MEAlertLoggerTest *__shred_instance = nil;

    dispatch_once(&onceToken, ^{
        __shred_instance = [[MEAlertLoggerTest alloc] init];
    });
    
    return __shred_instance;
}

- (BOOL) alertLogger:(MEAlertLogger *)logger shouldShowHistoryWhenStatusIs:(NSInteger)status{
    return NO;
}

- (BOOL) alertLogger:(MEAlertLogger *)logger shouldShowMessageWhenStatusIs:(NSInteger)status{
    return NO;
}

- (UIFont*) alertLogger:(MEAlertLogger *)view fontForStatus:(NSInteger)status{
    return [UIFont fontWithName:@"Optima" size:12];
}

- (NSInteger) alertLoggerDepth{
    return 30;
}

- (UIImage*) alertLogger:(MEAlertLogger *)view iconForStatus:(NSInteger)status{
    return [icons objectForKey:[NSNumber numberWithInteger:status]];
}

- (NSTimeInterval) alertLogger:(MEAlertLogger *)view timeOutForStatus:(NSInteger)status{
    return 10;
}

@end


@interface ViewController ()

@property (nonatomic,strong) MEAlertLoggerTest *alertLogger;

@end

@implementation ViewController
{
    NSTimer *eventTimer;
    NSInteger number;
}


- (void) seedEvent: (NSTimer*)timer{
    
    
    static int start_from = 3;
    NSTimeInterval sctime = arc4random() % 5;
    
    
    if (timer) {
        [timer invalidate];
    }
    eventTimer = [NSTimer scheduledTimerWithTimeInterval:sctime target:self selector:@selector(seedEvent:) userInfo:nil repeats:NO];
    
    MEAlertLoggerTest *logger = [MEAlertLoggerTest sharedInstance];
    
    MEAlertType atp = arc4random()%4+1;
    
    NSArray *messages = @[
                          @" (%i) Это разный набор сообщений, которые мы пытаемся кинуть в журнал. Событие наступило через %i cек. после последнего, оно имеет статус %i",
                          @" (%i) Некоторое событие. Событие: %i cек. статус %i",
                          @" (%i) Какое-то событие которые мы потенциально показываем в этом логгере. Оно происходит асинхронно и также асинхронно показывается в журнале сообщений. Это событие произошло через %i со статусом: %i. ",
                          @" (%i) Вот еще событие: %i/%i."
                          ];
    
    NSInteger num = arc4random() % (messages.count-1);
    
    if (start_from-->=0) {
        //return;
    }
    
    [logger pushMessageText:[NSString stringWithFormat:messages[num], number, (int)sctime, (int)atp] withStatus:(MEAlertType)atp];
    
    number++;
}

- (MEAlertLoggerTest*)alertLogger{
    if (!_alertLogger) {
        _alertLogger = [MEAlertLoggerTest sharedInstance];
    }
    return _alertLogger;
}


- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.alertLogger pushMessageText:@"test message with status 3. test message with status 3. test message with status 3. test message with status 3. test message with status 3. test message with status 3. test message with status 3. " withStatus:3];
    });
    
    [self seedEvent:nil];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blueColor];
        
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)run:(UIButton *)sender {
    NSLog(@" .... RUN .... ");
    if (self.alertLogger.isPresented) {
        [self.alertLogger hide];
    }
    else{
        [self.alertLogger show];
    }
}
@end
