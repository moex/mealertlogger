//
//  MEQueue.m
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/21/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import "_MEQueue.h"

@implementation _MEQueue
{
    NSMutableArray *queue;
}

- (NSArray*) array{
    return queue;
}

- (id) init{
    self = [super init];
    if(self){
        queue = [NSMutableArray arrayWithCapacity:0];
        _depth = 0;
    }
    return self;
}

- (void) setDepth:(NSUInteger)size{
    @synchronized (self){
        _depth=size;
        if ([queue count]>_depth) {
            [queue removeObjectsInRange:NSMakeRange(_depth, [queue count]-_depth)];
        }
    }
}

- (id) lastObject{
    return [queue lastObject];
}

- (id) pop{
    @synchronized (self){
        if ([queue count]==0){
            return nil;
        }
        @try {
            id r_ = [queue objectAtIndex:0];
            [queue removeObjectAtIndex:0];
            return r_;
        }
        @catch (NSException *exception) {
            NSLog(@"%@ at %s:%i", exception, __FILE__, __LINE__);
            return nil;
        }
        return nil;
    }
}

- (void) push:(id)object{
    @synchronized (self){
        if (!object)
            return;
        [queue addObject:object];
        if (_depth>=1 && queue.count>_depth) {
            [queue removeObjectsInRange:NSMakeRange(0, queue.count-_depth)];
        }
    }
}

@end

