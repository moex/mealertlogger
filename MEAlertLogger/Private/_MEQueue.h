//
//  MEQueue.h
//  MExchangeInfo-Universal
//
//  Created by denis svinarchuk on 11/21/13.
//  Copyright (c) 2013 Moscow Exchange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface _MEQueue : NSObject
@property(nonatomic) NSUInteger depth;
@property(readonly) NSArray *array;
- (void) push:(id)object;
- (id) pop;
- (id) lastObject;
@end
