//
//  MEAlertItem.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 22.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "MEAlertItem.h"

@implementation MEAlertItem
{
    UIFont *font;
}

- (BOOL) isEqual:(MEAlertItem*)object{
    return [_text isEqualToString:object->_text] && self.status==object.status;
}

- (id) initWithText:(NSString*)text withStatus:(NSInteger)status withFont:(UIFont*)afont{
    self = [super init];
    
    if (self) {
        _time = [NSDate dateWithTimeIntervalSince1970:time(0)];
        _text = text;
        _status = status;
        font = afont;
    }
    
    return self;
}


- (NSString*) text{
    NSDateFormatter *df = [[NSDateFormatter alloc] init]; [df setDateFormat: [NSDateFormatter dateFormatFromTemplate:@"HH:mm:ss" options:0 locale:[NSLocale currentLocale]]];
    return [NSString stringWithFormat:@"%@: %@", [df stringFromDate:_time], _text ];
}

- (NSAttributedString*) attributedText{
    NSDateFormatter *df = [[NSDateFormatter alloc] init]; [df setDateFormat: [NSDateFormatter dateFormatFromTemplate:@"HH:mm:ss" options:0 locale:[NSLocale currentLocale]]];
    
    //
    // time attributes
    //
    NSMutableAttributedString *_time_string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"   %@  ", [df stringFromDate:_time]]];
    
    if (font) {
        [_time_string addAttribute:NSFontAttributeName value:[UIFont fontWithName:font.fontName size:font.pointSize-2.] range:NSMakeRange(0, _time_string.length)];
    }
    [_time_string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithWhite:0.1 alpha:0.6] range:NSMakeRange(0, _time_string.length)];
    
    //
    // message
    //
    NSMutableAttributedString *_message_string = [[NSMutableAttributedString alloc] initWithString:_text];
    
    if (font) {
        [_message_string addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, _message_string.length)];
    }
    [_message_string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithWhite:0.0 alpha:0.9] range:NSMakeRange(0, _message_string.length)];
    
    //
    // paragraph
    //
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.headIndent  = 10; // <--- indention if you need it
    paragraphStyle.lineSpacing = 1; // <--- magic line spacing here!
    
    [_message_string addAttribute: NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, _message_string.length)];
    
    [_time_string appendAttributedString:_message_string];
    
    return _time_string;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%li: %@ :%@", (long)_status, _text, [_time descriptionWithLocale:[NSLocale currentLocale]]];
}
@end
