//
//  MEAlertWarnIconController.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEAlertViewController.h"

static const CGFloat kMEAlertLogger_IndentSize  = 5;

@interface MEAlertWarnIconController : MEAlertViewController
@property (nonatomic,strong)   UIImage *warnIcon;
@end
