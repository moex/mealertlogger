//
//  MEAlertWarnIconController.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "MEAlertWarnIconController.h"

@interface MEAlertWarnIconController ()
@property (nonatomic,strong)   UIImageView *warnIconView;
@end

@implementation MEAlertWarnIconController

- (CGRect) openFrameInView:(UIView*)containerView{
    CGRect frame = containerView.bounds;
    
    CGSize iconSize = self.warnIconView.bounds.size;
    
    frame.origin.y = frame.size.height - iconSize.height - kMEAlertLogger_IndentSize;
    frame.origin.x = frame.size.width - iconSize.width - kMEAlertLogger_IndentSize;
    frame.size.width = iconSize.width;
    frame.size.height = iconSize.height;
    
    return frame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor   = [UIColor clearColor];
    
    [self.view addSubview:self.warnIconView];
    
    __weak typeof(self) this = self;
    
    self.transitioning.openFrame = ^(UIView* containerView){
        return [this openFrameInView:containerView];
    };
    
    self.transitioning.closeFrame = ^(UIView* containerView){
        CGRect frame = [this openFrameInView:containerView];
        frame.origin.y += self.warnIconView.bounds.size.height + kMEAlertLogger_IndentSize;
        return frame;
    };
}


- (UIImageView*) warnIconView{
    if (!_warnIconView) {
        _warnIconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        _warnIconView.backgroundColor = [UIColor clearColor];
    }
    return _warnIconView;
}

- (void) setWarnIcon:(UIImage *)warnIcon{
    self.warnIconView.image = warnIcon;
    self.warnIconView.frame = (CGRect){self.warnIconView.bounds.origin,warnIcon.size};
}

- (UIImage*) warnIcon{
    return self.warnIconView.image;
}

@end
