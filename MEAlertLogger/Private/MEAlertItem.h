//
//  MEAlertItem.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 22.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

@import UIKit;

@interface MEAlertItem : NSObject
- (id) initWithText:(NSString*)text withStatus:(NSInteger)status withFont:(UIFont*)afont;

@property(nonatomic, readonly) NSAttributedString *attributedText;
@property(nonatomic, strong)   NSString  *text;
@property(nonatomic, readonly) NSDate    *time;
@property(nonatomic, assign)   NSInteger status;
@end
