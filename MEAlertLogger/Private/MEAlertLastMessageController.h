//
//  MEAlertLastMessageController.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MEAlertWarnIconController.h"
#import "MEAlertViewController.h"
#import "MEAlertItem.h"
#import "_MEQueue.h"

@class MEAlertLogger;

@interface MEAlertLastMessageController : MEAlertViewController
@property (nonatomic,weak)   MEAlertWarnIconController  *warnIconController;
@property (nonatomic,weak)  MEAlertLogger      *alertLogger;

@property (nonatomic,strong) _MEQueue                   *messagesQ;
@property (nonatomic,strong) UIFont                     *font;

- (void) pushItem:(MEAlertItem*)item;

@property (nonatomic, copy) void (^dissmiss)();
@property (nonatomic, copy) void (^presentLog)();
@property (nonatomic, copy) void (^dismessLog)();

@end

