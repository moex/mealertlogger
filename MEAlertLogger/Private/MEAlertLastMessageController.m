//
//  MEAlertLastMessageController.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "MEAlertLastMessageController.h"
#import "MEAlertLogger.h"

@interface MEAlertLastMessageController () <UITableViewDataSource>

@property (nonatomic,strong) NSAttributedString    *message;

@property (nonatomic,readonly) CGFloat              height;
@property (nonatomic,strong)   UITextView          *textView;
@property (nonatomic,strong)   UIView              *containerView;
@property (nonatomic,strong)   UIVisualEffectView  *blurView;
@property (nonatomic,strong)   UIVisualEffectView  *blurLogView;

@property (nonatomic,strong) UITableView  *messagesLogView;

@property (nonatomic,readonly) MEAlertItem *lastItem;

@end

@implementation MEAlertLastMessageController
{
    BOOL  layoutIsUpdating;
    BOOL  isOpened;
}

@synthesize height=_height;

#pragma mark -
#pragma mark - Push

- (MEAlertItem*) lastItem{
    return self.messagesQ.lastObject;
}

- (_MEQueue*) messagesQ{
    if (!_messagesQ) {
        _messagesQ = [[_MEQueue alloc] init];
        _messagesQ.depth = 10;
    }
    return _messagesQ;
}

- (void) pushItem:(MEAlertItem *)item{
    [self.messagesQ push:item];
    self.message = item.attributedText;
    [self messageTableReload];
}



#pragma mark -
#pragma mark - Layouts 
- (CGFloat) height{
    
    if (isOpened) {
        CGRect frame = [UIScreen mainScreen].bounds;
        return frame.size.height - [UIApplication sharedApplication].statusBarFrame.size.height - 2 * kMEAlertLogger_IndentSize;
    }
    
    if (_height<=0) {
        return 96;
    }
    return _height;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isOpened = NO;
}

- (UIView*) containerView{
    return self.view.superview;
}

- (void) viewWillChangeSize:(CGSize)size{
    [super viewWillChangeSize:size];
    CGSize iconSize = self.warnIconController.warnIcon.size;
    self.textView.frame = CGRectMake(0, 0, size.width-iconSize.width - 3 * kMEAlertLogger_IndentSize, 1);
    [self.textView sizeToFit];
}

- (CGRect) openFrameInView:(UIView*)containerView{
    
    CGRect frame = containerView.bounds;
    CGSize iconSize = self.warnIconController.warnIcon.size;

    frame.size.width  = frame.size.width - iconSize.width - 3 * kMEAlertLogger_IndentSize;    
    frame.origin.y    = frame.size.height - self.height - kMEAlertLogger_IndentSize;
    frame.origin.x    = kMEAlertLogger_IndentSize;
    frame.size.height = self.height;
    
    if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
        frame.size.width = [[UIApplication sharedApplication] keyWindow].bounds.size.width/2.0;
        frame.origin.x =  [[UIApplication sharedApplication] keyWindow].bounds.size.width - frame.size.width - 2 * kMEAlertLogger_IndentSize - iconSize.width;
    }
    
    return frame;
}


- (void) viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self updateTextViewLayout];
}

- (CGRect) logViewFrame{
    CGRect frame = self.view.bounds;
    if (!isOpened) {
        frame.origin.y = self.textView.frame.origin.y;
        frame.size.height = 0.0;
    }
    else{
        frame.size.height -= self.textView.frame.size.height + kMEAlertLogger_IndentSize;
    }
    return frame;
}

- (void) updateViewLayout{
    self.messagesLogView.frame = self.blurLogView.frame = [self logViewFrame];
}

#pragma mark -
#pragma mark - Contruction
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor clearColor];
    self.view.layer.cornerRadius = 2;
    self.view.layer.masksToBounds = YES;
    
    [self.view insertSubview:self.blurView atIndex:0];


    __weak typeof(self) this = self;
    
    self.transitioning.openFrame = ^(UIView* containerView){
        return [this openFrameInView:containerView];
    };

    self.transitioning.closeFrame = ^(UIView* containerView){
        CGRect frame =  [this openFrameInView:containerView];
        
        
        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
            frame.origin.x =  [[UIApplication sharedApplication] keyWindow].bounds.size.width;
        }
        else{
            frame.origin.y += frame.size.height + kMEAlertLogger_IndentSize;
        }
        
        return frame;
    };
    
    self.open = ^(){
        if (
            [this.alertLogger.delegate respondsToSelector:@selector(alertLogger:shouldShowHistoryWhenStatusIs:)]
            &&
            ![this.alertLogger.delegate alertLogger:self.alertLogger shouldShowHistoryWhenStatusIs:self.lastItem.status]
            ) {
            if (this.dissmiss) {
                this.dissmiss();
            }
            return ;
        }

        
        isOpened = YES;

        if (this.presentLog) {
            this.presentLog();
        }
        
        this.view.frame = [this openFrameInView:[[UIApplication sharedApplication] keyWindow]];
        this.blurLogView.alpha = 0.0;
        
        CGRect frame = [this textViewFrame];
        frame.size.height = 0.0;
        this.messagesLogView.frame = this.blurLogView.frame = frame;
        
        [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                         animations:^{
                             this.messagesLogView.alpha = this.blurLogView.alpha = 1.0;
                             [this updateViewLayout];
                         }
         ];
    };
    
    self.close = ^(){
                
        if (
            [this.alertLogger.delegate respondsToSelector:@selector(alertLogger:shouldShowHistoryWhenStatusIs:)]
            &&
            ![this.alertLogger.delegate alertLogger:self.alertLogger shouldShowHistoryWhenStatusIs:self.lastItem.status]
            ) {
            if (this.dissmiss) {
                this.dissmiss();
            }
            return ;
        }

        
        if (this.dismessLog) {
            this.dismessLog();
        }
        
        isOpened = NO;
        [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                         animations:^{
                             this.messagesLogView.alpha = this.blurLogView.alpha = 0.0;
                             [this updateViewLayout];
                         }
                         completion:^(BOOL finished) {
                             this.view.frame = [this openFrameInView:[[UIApplication sharedApplication] keyWindow]];
                         }
         ];
    };
}


#pragma mark - 
#pragma mark - Visual effects

- (UIVisualEffectView*)blurView{
    if (!_blurView) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        _blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _blurLogView.autoresizingMask = UIViewAutoresizingNone;
        [_blurView setFrame:self.view.bounds];
    }
    return _blurView;
}

- (UIVisualEffectView*)blurLogView{
    if (!_blurLogView) {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        _blurLogView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        _blurLogView.backgroundColor = [UIColor clearColor];
        [_blurLogView setFrame:[self logViewFrame]];
        _blurLogView.autoresizingMask = UIViewAutoresizingNone;
        [self.view insertSubview:_blurLogView belowSubview:self.textView];
    }
    return _blurLogView;
}

#pragma mark - 
#pragma mark - Last message text view

- (UITextView*) textView{
    if (!_textView) {
        _textView = [[UITextView alloc] init];
        _blurLogView.autoresizingMask = UIViewAutoresizingNone;

        _textView.backgroundColor = [UIColor clearColor];
        
        _textView.scrollEnabled = NO;
        _textView.editable      = NO;
        _textView.textAlignment = NSTextAlignmentNatural;
        _textView.contentMode   = UIViewContentModeScaleToFill;
        
        _textView.textColor = [UIColor whiteColor];

        [self.view addSubview:_textView];
    }
    
    return _textView;
}

- (void) setFont:(UIFont *)font{
    self.textView.font = font;
}

- (UIFont*) font{
    return self.textView.font;
}

- (NSAttributedString*) message{
    return self.textView.attributedText;
}

- (CGRect) textViewFrame{
    CGRect frame = self.view.bounds;
    frame.size = self.textView.contentSize;
    frame.origin.y = self.view.bounds.size.height-frame.size.height;
    return frame;
}

- (void) updateTextViewLayout{
    [self.textView sizeToFit];
    self.textView.contentSize = CGSizeMake(self.view.bounds.size.width, self.textView.contentSize.height);
    self.textView.frame = [self textViewFrame];
    
    CGRect frame = self.textView.frame;
    frame.size.width = self.view.bounds.size.width;
    self.blurView.frame = frame;
    self.messagesLogView.frame = self.blurLogView.frame = [self logViewFrame];
}

- (void) setMessage:(NSAttributedString *)message{
    self.textView.attributedText = message;
    
    [UIView animateWithDuration:[UIApplication sharedApplication].statusBarOrientationAnimationDuration
                     animations:^{
                         [self updateTextViewLayout];
                     }
     ];
}

#pragma mark - 
#pragma mark Messages log 

- (UITableView*) messagesLogView{
    if (!_messagesLogView) {
        
        _messagesLogView = [[UITableView alloc] initWithFrame:self.blurLogView.bounds style:UITableViewStylePlain];
        _messagesLogView.autoresizingMask = UIViewAutoresizingNone;
        
        _messagesLogView.dataSource = self;
        _messagesLogView.rowHeight = UITableViewAutomaticDimension;
        _messagesLogView.estimatedRowHeight = 160;
        _messagesLogView.backgroundColor = [UIColor clearColor];
        _messagesLogView.separatorInset = UIEdgeInsetsZero;
        _messagesLogView.separatorColor = [UIColor clearColor];
        _messagesLogView.scrollEnabled = YES;
        
        [self.blurLogView addSubview:_messagesLogView];
    }
    return _messagesLogView;
}

#pragma mark -
#pragma mark - UITableView source

- (void) messageTableReload{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.messagesLogView && self.messagesQ.array.count>=2) {
            [self.messagesLogView setNeedsLayout];
            [UIView transitionWithView:self.messagesLogView
                              duration:0.1
                               options:UIViewAnimationOptionCurveLinear
                            animations:^{
                                [self.messagesLogView reloadData];
                                NSIndexPath *path = [NSIndexPath indexPathForRow:self.messagesQ.array.count-2 inSection:0];
                                [self.messagesLogView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                                [self.messagesLogView layoutIfNeeded];
                            }
                            completion:nil];
        }
    });
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return self.messagesQ.array.count<=1?0:1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messagesQ.array.count<=1?0:self.messagesQ.array.count-1;
}

- (MEAlertItem*) itemForIndexPath:(NSIndexPath*)indexPath{
    return self.messagesQ.array[indexPath.row];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    MEAlertItem *item = [self itemForIndexPath:indexPath];
    
    UIFont *font = self.font;
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.numberOfLines = 20;
    }
    
    if ([self.alertLogger.delegate respondsToSelector:@selector(alertLogger:fontForStatus:)]) {
        font = [self.alertLogger.delegate alertLogger:self.alertLogger fontForStatus:item.status];
    }
    
    cell.textLabel.font = font;
    cell.textLabel.attributedText = item.attributedText;
    
    //
    //  TODO...
    //
    //
    //    if (self.alertLogger.delegate && [self.alertLogger.delegate respondsToSelector:@selector(alertLogger:iconForStatus:)]) {
    //
    //        UIImage *image = [self.alertLogger.delegate alertLogger:self.alertLogger iconForStatus:item.status];
    //
    //        UIImageView *iview = [textView viewWithTag:28];
    //
    //        if (!iview) {
    //            iview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, font.pointSize+2., font.pointSize+2.)];
    //        }
    //
    //        iview.image = image;
    //        iview.frame = CGRectMake(textView.textContainerInset.left+5.0,
    //                                 textView.textContainerInset.top+2.0,
    //                                 iview.bounds.size.width, iview.bounds.size.height);
    //
    //        UIBezierPath * imgRect = [UIBezierPath bezierPathWithRect:iview.frame];
    //        textView.textContainer.exclusionPaths = @[imgRect];
    //        iview.tag = 42;
    //        
    //        [textView addSubview:iview];
    //    }
    return cell;
    
}

@end
