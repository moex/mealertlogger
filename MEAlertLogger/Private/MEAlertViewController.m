//
//  MEAlertViewController.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "MEAlertViewController.h"


@interface MEAlertTransition()
@property (nonatomic,weak)     MEAlertViewController *controller;
- (CGRect) openFrameInView:(UIView*) containerView;
- (CGRect) closeFrameInView:(UIView*) containerView;
@end

@interface MEAlertViewController ()
@end

@implementation MEAlertViewController
{
    //BOOL isOpened;
}

@synthesize transitioning=_transitioning;
@synthesize isPresented=_isPresented;

- (BOOL) isPresented{
    if (_isPresented || self.transitioning.isPresented) {
        return YES;
    }
    return NO;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _isPresented = NO;
    _isOpened = NO;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _isPresented = NO;
    _isOpened = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    tapGesture.enabled = YES;
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    
    [self.view addGestureRecognizer:tapGesture];
}

- (void) tapHandler:(UITapGestureRecognizer*)gesture{
        
    if (!_isOpened) {
        if (self.open) {
            self.open();
        }
        _isOpened = YES;
    }
    else{
        if (self.close) {
            self.close();
        }
        _isOpened = NO;
    }
}

- (MEAlertTransition*)transitioning{
    if (!_transitioning) {
        _transitioning = [[MEAlertTransition alloc] init];
        _transitioning.controller = self;
    }
    return _transitioning;
}

- (void) viewWillChangeSize:(CGSize)size{}

@end


//
//
// Transitioning
//
//

@implementation MEAlertTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return [UIApplication sharedApplication].statusBarOrientationAnimationDuration;
}

- (CGRect) openFrameInView:(UIView*) containerView{
    CGRect frame;
    if (self.openFrame)
        frame = self.openFrame(containerView);
    else
        frame = containerView.bounds;
    return frame;
}

- (CGRect) closeFrameInView:(UIView *)containerView{
    CGRect frame;
    if (self.closeFrame)
        frame = self.closeFrame(containerView);
    else{
        frame = [self openFrameInView:containerView];
        frame.origin.y += frame.size.height;
    }
    return frame;
}

- (void) animatePresentedTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    self.isPresented=YES;
    
    UIViewController *toViewController   = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    
    CGRect frame = [self openFrameInView:containerView];
    
    CGRect startFrame = [self closeFrameInView:containerView];
        
    toViewController.view.frame = startFrame;
    
    toViewController.view.alpha = 0.0;
    [containerView addSubview:toViewController.view];
    
    CGFloat duration = [self transitionDuration:transitionContext];
    CGFloat spring = duration * 2;
    CGFloat velocity = spring * 2;

    [UIView animateWithDuration:duration
                          delay:0.0
         usingSpringWithDamping:spring
          initialSpringVelocity:velocity
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         toViewController.view.frame = frame;
                         toViewController.view.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         [transitionContext completeTransition:YES];
                     }
     ];
}

- (void) animateDismissedTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    
    CGRect frame = [self closeFrameInView:containerView];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]
                     animations:^{
                         fromViewController.view.frame = frame;
                         fromViewController.view.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [fromViewController.view removeFromSuperview];
                         [transitionContext completeTransition:YES];
                         self.isPresented=NO;
                     }];
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    if (!self.isPresented)
        [self animatePresentedTransition:transitionContext];
    else
        [self animateDismissedTransition:transitionContext];
}


@end


//
//
// Presenting
//
//

@implementation MEAlertPresentation
- (void) containerViewWillLayoutSubviews{
    MEAlertViewController *controller = (MEAlertViewController*)self.presentedViewController;
        
    [controller viewWillChangeSize:self.containerView.superview.bounds.size];
    
    self.containerView.frame = [controller.transitioning openFrameInView:[[UIApplication sharedApplication] keyWindow]];
    self.presentedView.frame = self.containerView.bounds;
}

- (void) dismissalTransitionDidEnd:(BOOL)completed{
    self.containerView.bounds = self.containerView.superview.bounds;
    self.presentedView.frame = self.containerView.bounds;
}

@end
