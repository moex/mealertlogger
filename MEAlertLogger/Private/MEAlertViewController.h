//
//  MEAlertViewController.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 24.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MEAlertTransition : NSObject <UIViewControllerAnimatedTransitioning>
@property (atomic,assign)    BOOL   isPresented;
@property (nonatomic, copy)  CGRect  (^openFrame)(UIView *inView);
@property (nonatomic, copy)  CGRect  (^closeFrame)(UIView *inView);
@end

@interface MEAlertPresentation : UIPresentationController
@end

@interface MEAlertViewController : UIViewController
@property (nonatomic,readonly) MEAlertTransition   *transitioning;
@property (nonatomic,readonly) BOOL isPresented;
@property (nonatomic, copy) void (^open)();
@property (nonatomic, copy) void (^close)();
@property (nonatomic, assign)  BOOL isOpened;
- (void) viewWillChangeSize:(CGSize)size;
@end
