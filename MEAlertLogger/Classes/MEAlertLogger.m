//
//  MEAlertLoggerViewViewController.m
//  MEAlertLogger
//
//  Created by denis svinarchuk on 21.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

#import "MEAlertLogger.h"
#import "MEAlertItem.h"
#import "_MEQueue.h"

#import "MEAlertWarnIconController.h"
#import "MEAlertLastMessageController.h"

#define ME_ALERT_UI_ALERT_FONT  @"HelveticaNeue-Light"


@interface MEAlertLogger () <UIViewControllerTransitioningDelegate>

@property (nonatomic,strong) MEAlertWarnIconController     *warnIconController;
@property (nonatomic,strong) MEAlertLastMessageController  *lastMessageController;
@property (nonatomic,strong) MEAlertItem                   *lastMessageItem;

@property (nonatomic,strong) UIFont  *font;

@end

@implementation MEAlertLogger
{
    NSTimer    *hideTimer;
}

- (void) pushMessageText:(NSString *)text withStatus:(NSInteger)status{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
               
        if (self.delegate && [self.delegate respondsToSelector:@selector(alertLoggerDepth)]) {
            if (self.lastMessageController.messagesQ.depth != [self.delegate alertLoggerDepth]) {
                self.lastMessageController.messagesQ.depth = [self.delegate alertLoggerDepth];
           }
        }
        
        UIFont *font = self.font;
        if (self.delegate && [self.delegate respondsToSelector:@selector(alertLogger:fontForStatus:)]) {
            font = [self.delegate alertLogger:self fontForStatus:status];
        }
        self.lastMessageController.font = font;
        
        self.lastMessageItem = [[MEAlertItem alloc] initWithText:text withStatus:status withFont:font];

        MEAlertItem *previouseMessageItem = [self.lastMessageController.messagesQ lastObject];
        
        if ([previouseMessageItem isEqual:self.lastMessageItem]){
            [self showLastMessage];
            return ;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(alertLogger:iconForStatus:)]) {
            UIImage *image = [self.delegate alertLogger:self iconForStatus:status];
            self.warnIconController.warnIcon = image;
        }
        
        [self.lastMessageController pushItem:self.lastMessageItem];
        
        [self showLastMessage];
    });
}


#pragma mark - 
#pragma mark - Presentation

- (void) hide{
    
    if (self.lastMessageController.isPresented) {
        [self.lastMessageController dismissViewControllerAnimated:YES completion:^{
            [self.warnIconController dismissViewControllerAnimated:YES completion:^{
                [self.view removeFromSuperview];
            }];
        }];
    }
    else if (!self.warnIconController.isBeingDismissed && self.warnIconController.isPresented) {
        [self.warnIconController dismissViewControllerAnimated:YES completion:^{
            [self.view removeFromSuperview];
        }];
    }
}

- (void) show{
    [self showLastMessage];
}

- (void) showLastMessage{

    [self startAlertIconHiddingTimer:self.lastMessageItem.status];

    if (!self.warnIconController.isBeingPresented && !self.warnIconController.isPresented) {
        
        self.view.backgroundColor = [UIColor clearColor];
        self.view.alpha = 0.0;
        self.view.opaque = NO;
        
        [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
        
        [self presentViewController:self.warnIconController animated:YES completion:^{
            if ([self.delegate respondsToSelector:@selector(alertLogger:mustShowMessageWhenStatusIs:)]) {
                if ([self.delegate alertLogger:self mustShowMessageWhenStatusIs:self.lastMessageItem.status]) {
                    if (!self.lastMessageController.isPresented) {
                        [self.warnIconController presentViewController:self.lastMessageController animated:YES completion:^{
                            self.warnIconController.isOpened = YES;
                        }];
                    }
                }
            }
        }];
    }
    else {
        if ([self.delegate alertLogger:self mustShowMessageWhenStatusIs:self.lastMessageItem.status]) {
            if (!self.lastMessageController.isPresented) {
                [self.warnIconController presentViewController:self.lastMessageController animated:YES completion:^{
                    self.warnIconController.isOpened = YES;
                }];
            }
        }
    }
}

- (void) startAlertIconHiddingTimer:(NSInteger)status{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (hideTimer) {
            [hideTimer invalidate];
        }
        
        // Start time to hide
        if ([self.delegate respondsToSelector:@selector(alertLogger:timeOutForStatus:)]) {
            NSTimeInterval timeout = [self.delegate alertLogger:self timeOutForStatus:status];
            hideTimer = [NSTimer scheduledTimerWithTimeInterval:timeout target:self selector:@selector(hideAlertIcon:) userInfo:nil repeats:NO];
        }
        
    });
}

- (void) hideAlertIcon:(NSTimer*)timer{
    dispatch_async(dispatch_get_main_queue(), ^{

        if (timer)
            [timer invalidate];
        
        [self hide];
        
    });
}


#pragma mark -
#pragma mark - Delegates
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (id<UIViewControllerAnimatedTransitioning>) animationControllerForDismissedController:(UIViewController *)dismissed{
    if (dismissed==self.warnIconController) {
        return self.warnIconController.transitioning;
    }
    else if (dismissed==self.lastMessageController){
        return self.lastMessageController.transitioning;
    }
    return nil;
};

- (id<UIViewControllerAnimatedTransitioning>) animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    if (presented==self.warnIconController) {
        return self.warnIconController.transitioning;
    }
    else if (presented==self.lastMessageController){
        return self.lastMessageController.transitioning;
    }
    return nil;
}

- (UIPresentationController*) presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source{
    return [[MEAlertPresentation alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}


#pragma mark -
#pragma mark - Properties
- (UIFont*) font{
    if (!_font)
        _font = [UIFont fontWithName:ME_ALERT_UI_ALERT_FONT size:12];
    return _font;
}

- (BOOL) isPresented{
    return self.warnIconController.isPresented;
}


- (MEAlertWarnIconController*) warnIconController{
    if (!_warnIconController) {
        _warnIconController = [[MEAlertWarnIconController alloc] initWithNibName:nil bundle:nil];
        _warnIconController.modalPresentationStyle = UIModalPresentationCustom;
        _warnIconController.transitioningDelegate = self;
        
        __weak typeof(self) this = self;
        
        void  (^dismissWarn)() = ^void() {
            if (!this.warnIconController.isBeingDismissed && this.warnIconController.isPresented) {
                [this.warnIconController dismissViewControllerAnimated:YES completion:^{
                }];
            }
        };
        
        BOOL  (^dismissWarnIfNeed)() = ^BOOL() {
            if ([this.delegate respondsToSelector:@selector(alertLogger:shouldShowMessageWhenStatusIs:)]
                &&
                ![this.delegate alertLogger:self shouldShowMessageWhenStatusIs:self.lastMessageItem.status]
                ) {
                dismissWarn();
                return YES;
            }
            return NO;
        };
        
        _warnIconController.open = ^(){
            
            [this startAlertIconHiddingTimer:this.lastMessageItem.status];

            if (dismissWarnIfNeed()) {
                return ;
            }
            
            [this startAlertIconHiddingTimer:this.lastMessageItem.status];
        
            if (!this.lastMessageController.isBeingPresented && !this.lastMessageController.isPresented) {
                [this.warnIconController presentViewController:this.lastMessageController animated:YES completion:^{}];
            }
        };
        
        _warnIconController.close = ^(){

            if (dismissWarnIfNeed()) {
                return ;
            }

            [this startAlertIconHiddingTimer:this.lastMessageItem.status];
            
            if (!this.lastMessageController.isBeingDismissed && this.lastMessageController.isPresented) {
                [this.lastMessageController dismissViewControllerAnimated:YES completion:^{
                    dismissWarn();
                }];
            }
        };
    }
    return _warnIconController;
}

- (MEAlertLastMessageController*) lastMessageController{
    if (!_lastMessageController) {
        
        _lastMessageController = [[MEAlertLastMessageController alloc] initWithNibName:nil bundle:nil];
        _lastMessageController.alertLogger = self;
        _lastMessageController.warnIconController = self.warnIconController;
        _lastMessageController.modalPresentationStyle = UIModalPresentationCustom;
        _lastMessageController.transitioningDelegate = self;
        
        __weak typeof(self) this = self;

        _lastMessageController.dissmiss = ^(){
            if (!this.lastMessageController.isBeingDismissed && this.lastMessageController.isPresented) {
                [this.lastMessageController dismissViewControllerAnimated:YES completion:^{
                }];
            }
        };
        
        _lastMessageController.presentLog = ^(){
            [this startAlertIconHiddingTimer:this.lastMessageItem.status];
        };
        
        _lastMessageController.dismessLog = ^(){
            [this startAlertIconHiddingTimer:this.lastMessageItem.status];
        };
        
      }
    return _lastMessageController;
}


@end
