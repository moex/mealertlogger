//
//  MEAlertLoggerViewViewController.h
//  MEAlertLogger
//
//  Created by denis svinarchuk on 21.10.15.
//  Copyright © 2015 MOEX. All rights reserved.
//

@import UIKit;

@class MEAlertLogger;

/**
 *  Allert logger view configuration protocol.
 */
@protocol MEAlertLoggerDelegate <NSObject>

@optional

/**
 *  Message logger size of logg.
 *
 *  @return log depth.
 */
- (NSInteger) alertLoggerDepth;

/**
 *  This is a font which should be used to view the message with the status.
 *
 *  @param view   allert logger view.
 *  @param status alert status.
 *
 *  @return UIFont.
 */
- (UIFont*) alertLogger:(MEAlertLogger*)logger fontForStatus:(NSInteger)status;

/**
 *  This is a image which should be used to indicate the message event for the status.
 *
 *  @param view   alert logger view.
 *  @param status alert status.
 *
 *  @return UIImage.
 */
- (UIImage*) alertLogger:(MEAlertLogger*)logger iconForStatus:(NSInteger)status;

/**
 *  This is a time while the logger last icon shows on screen before hide for the status. The next time icon will be viewed when the new message will be pushed.
 *
 *  @param view   alert logger view.
 *  @param status alert status.
 *
 *  @return timeout interval.
 */
- (NSTimeInterval) alertLogger:(MEAlertLogger*)logger timeOutForStatus:(NSInteger)status;

/**
 *  If you want to show history of messages you should return YES for the current message status when user taps on last message view. If you do not want to show only last message just return NO.
 *
 *  @param view alert logger view.
 *  @param status last alert status.
 *
 *  @return YES/NO.
 */
- (BOOL) alertLogger:(MEAlertLogger*)logger shouldShowHistoryWhenStatusIs:(NSInteger)status;

/**
 *  Tells that messages with the status should be shown or not when user taps on warn icon. If a last message already shown any result this delegate is ignored.
 *
 *  @param view   alert logger view.
 *  @param status alert status.
 *
 *  @return YES/NO.
 */
- (BOOL) alertLogger:(MEAlertLogger*)logger shouldShowMessageWhenStatusIs:(NSInteger)status;


/**
 *  Tells that messages with the status must be shown as last message in last message view. If a last message already shown any result this delegate is ignored.
 *
 *  @param view   alert logger view.
 *  @param status alert status.
 *
 *  @return YES/NO.
 */
- (BOOL) alertLogger:(MEAlertLogger*)logger mustShowMessageWhenStatusIs:(NSInteger)status;


@end

@interface MEAlertLogger : UIViewController

/**
 * It shows is presented the messages or not.
 */
@property (nonatomic,assign) BOOL isPresented;

/**
 *  Alert logger delegate.
 */
@property id<MEAlertLoggerDelegate> delegate;

/**
 *  Push new message to log.
 *
 *  @param text   text.
 *  @param status message status.
 */
- (void) pushMessageText:(NSString*)text withStatus:(NSInteger)status;

/**
 * Show message view.
 */
- (void) show;

/**
 * Hide message view.
 */
- (void) hide;

@end
