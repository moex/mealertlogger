Pod::Spec.new do |s|

  s.name         = "MEAlertLogger"
  s.version      = "0.7.0"
  s.summary      = "MEAlertLogger another type AlertView does not show alert immediately, it just shows icon related to event/warn/error status."

  s.description  = <<-DESC
                   This is a kind of alert view has one exception: it logs alerts which is pushed to one shared instance of the view from application.

                   Often i use asynchronous network connection to servers generates many events to connection pipe. Application receives these events and show on screen, for instance.
                   But! The iOS created for mobile devices. This is a key feature means that some events can be broken, connection can be suddenly closed, or device loses carrier. 
                   In this case user usually can't understand what happens, sometimes he feels anger or rage aganst the application which uses. When i want to try to prevent such situation -
                   i just show why the application "glitches". UIKit offers standart UIAlertView solves the issue partially. But if alerts show too often new feels, new rage would boil up. 
                   
                   MEAlertLogger another type AlertView does not show alert immediately, it just shows icon related to event/warn/error status. User can touch icon and see what happens. 
                   If he whants to see history of events, because the application can received many alerts, he could tap again and see list of previous alerts.

                   DESC

  s.homepage     = "https://bitbucket.org/denn_nevera/mealertlogger"
  s.license      = { :type => 'GPL Version 3', :file => 'LICENSE' }
  s.author       = { "denis svinarchuk" => "denn.nevera@gmail.com" }
  s.platform     = :ios, '7.0'

  s.source       = { :git => "https://denn_nevera@bitbucket.org/denn_nevera/mealertlogger.git", :tag => "0.7.0" }

  s.source_files  = 'MEAlertLogger/Classes/*.{h,m}', 'MEAlertLogger/Private/*.{h,m}'
  s.public_header_files = 'MEAlertLogger/Classes/*.h'
  #s.exclude_files =

  s.requires_arc = true

end
