# MEAlertLogger

Created by **denn.nevera**

This is a kind of alert view has one exception: it logs alerts which is pushed to one shared instance of the view from application.

Often i use asynchronous network connection to servers generates many events to connection pipe. Application receives these events and show on screen, for instance.
But! The iOS created for mobile devices. This is a key feature means that some events can be broken, connection can be suddenly closed, or device loses carrier.
In this case user usually can't understand what happens, sometimes he feels anger or rage aganst the application which uses. When i want to try to prevent such situation -
i just show why the application "glitches". UIKit offers standart UIAlertView solves the issue partially. But if alerts show too often new feels, new rage would boil up.

MEAlertLogger another type AlertView does not show alert immediately, it just shows icon related to event/warn/error status. User can touch icon and see what happens.
If he whants to see history of events, because the application can received many alerts, he could tap again and see list of previous alerts.

The project code is used as a part of Moscow Exchange mobile projects.


# How to use

* Install cocoapods: http://cocoapods.org/
* Edit Podfile in the your XCode project directory YourApp:
```
    $ edit Podfile        
    platform :ios, '7.0'
    pod 'MEAlertLogger', :git => 'https://denn_nevera@bitbucket.org/denn_nevera/mealertlogger.git', :branch => 'master'
```

* Install dependences in your project
```
    $ pod install
```

* Open the XCode workspace instead of the project file
```
    $ open YourApp.xcworkspace
```

* Import API
```
#!objective-c
#import "MEAlertLogger.h"

```

* Example
```
#!objective-c

#import "MEAlertLogger.h"

typedef enum {
    ME_ALERT_WARN=1,
    ME_ALERT_NETWORK_OK=2,
    ME_ALERT_NETWORK_LOST=3,
    ME_ALERT_FATAL=4
} MEAlertType;

@interface MEAlertLoggerTestView : MEAlertLoggerView <MEAlertLoggerDelegate>
@end

static MEAlertLoggerTestView *__shred_instance = nil;

@implementation MEAlertLoggerTestView
{
    NSDictionary *icons;
}

- (id) init{
    self = [super init];

    if (__shred_instance) {
        self = __shred_instance;
        return self;
    }

    if (self) {
        icons = @{
                  @1: [UIImage imageNamed:@"alert_warn"],
                  @2: [UIImage imageNamed:@"alert_network_ok"],
                  @3: [UIImage imageNamed:@"alert_network_lost"],
                  @4: [UIImage imageNamed:@"alert_fatal"]
                  };
        self.delegate = self;
    }
    
    return self;
}

+ (id) sharedInstance{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __shred_instance = [[MEAlertLoggerTestView alloc] init];
    });
    
    return __shred_instance;
}

- (UIFont*) alertLoggerView:(MEAlertLoggerView *)view fontForStatus:(NSInteger)status{
    return [UIFont fontWithName:@"Optima" size:12];
}

- (NSInteger) alertLoggerDepth{
    return 10;
}

- (UIImage*) alertLoggerView:(MEAlertLoggerView *)view iconForStatus:(NSInteger)status{
    return [icons objectForKey:[NSNumber numberWithInteger:status]];
}

- (NSTimeInterval) alertLoggerView:(MEAlertLoggerView *)view timeOutForStatus:(NSInteger)status{
    return 1;
}

@end

...

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    MEAlertLoggerTestView *logger = [MEAlertLoggerTestView sharedInstance];
    [logger pushMessageText:@"Application appeared." withStatus:ME_ALERT_WARN];
}


```



